jQuery(document).ready(function($) {

	$('#mini-portfolio').dataTable( {
	    paging: false
	});

	var stages = new Object();

	$(window).load(function() {

		$( '.stage' ).each( function() {
			var stage_name = $( this ).attr('id');
			stages[stage_name] = new Stage($(this));
			stages[stage_name].init();
			stages[stage_name].activate();
		});

		pajax.init('*[c5-ajax-target]');
		pajax.activate();

	});

	function Stage( the_stage ) {

		return {

//			el: element,
			open: false,
			jel: the_stage,

			init: function() {
				var self = this;

//				self.jel = $(element);
				if( self.jel.length ) {
					if( self.jel.css('display') == 'none' )
						self.open = false;
				}
			},

			activate: function() {
				var self = this;

//				self.jel.css('height', 239);
				self.jel.slideDown('slow', function() {
				});
			},

			show: function() {
				var self = this;

				if( self.open !== true ) {
					self.jel.find('img').animate({opacity: 0}, 'slow');
					self.jel.animate({'min-height':462}, 'slow', function() {
					});
					self.open = true;
				} else {
					//self.sleep();
				}
			},

			sleep: function() {
				var self = this;

				if( self.open !== true ) {
					wistiaEmbed = self.jel.find('iframe')[0].wistiaApi;
					wistiaEmbed.pause();
				}
			}
		}
	}

	var pajax = {

		ajax_target:'',
		ajax_link: '',
		triggers: '',
		stage_view: '',

		init: function( trigger ) {
			var self = this;
			self.triggers = trigger;
			$('body').on('click', self.triggers, function(e) {
					e.preventDefault();
					if( $(this).hasClass('ajax-loaded') ) return;
					self.ajax_link = $(this).attr('c5-ajax-url') + "?t=pajax";
					self.stage_view = $(this).closest('.c5-view-stage');
					var sn = self.stage_view.find('.stage').attr('id');
					stages[sn].show();
					self.ajax_target = self.stage_view.find( $(this).attr('c5-ajax-target') );
					self.load( $(this) );
			});
		},

		activate: function () {
			var self = this;
			var active_tabs = $(self.triggers).parent('li.active');
			var active_triggers = active_tabs.find('a');
			active_triggers.trigger('click');
		},

		load: function( cur_trigger ) {
			var self = this;
			self.ajax_target.load( self.ajax_link, function() {
//				self.ajax_target.find('.c5-ajax-content').animate('opacity', 1);
//				cur_trigger.addClass('ajax-loaded');
			});
		}
	}

	$('.sla-trade-alert').on('click', '.ta-view-analysis-chart', function(e) {

		var the_layer = $(this).closest('.cfo-trade-alert');

		// animate close the sections not needed
		the_layer.find('> .c5-body .c5-view-section').slideUp('fast');

		var anal_sect = the_layer.find('> .c5-body .ta-view-chart');

		anal_sect.slideDown('slow');

//		var chart_code = $(this).data('chart-id');

//		var chart_target = the_layer.find('.chart-target').attr('id');

//		showChartWidget( chart_code, chart_target );
	});

	$('.cashflowfx-diary').on('click', '.ta-view-chart .c5-close', function(e) {

		var anal_sect = $('.c5-view-full.cfo-trade-alert > .c5-body .ta-view-chart');
		anal_sect.slideUp('fast');

		// animate close the sections not needed
		$('.c5-view-full.cfo-trade-alert > .c5-body .c5-view-section').not('.ta-view-chart').slideDown('fast');

	});


});

