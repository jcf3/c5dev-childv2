<?php
$c5Version = "1.0";

function child_styles() {

	if(is_admin()) return;

	//	Register CSS Files
	//	Example:
	//	wp_register_style( 'countdown',
	//					get_stylesheet_directory_uri() . '/lib/jquery.countdown/jquery.countdown.css');
	//	wp_enqueue_style( 'countdown' );

	//	echo "<link href='http://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700,800' rel='stylesheet' type='text/css'>";
	echo '<link href="//netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.css" rel="stylesheet">';
}
add_action( 'wp_enqueue_scripts', 'child_styles' );


function child_admin_styles() {

//	wp_register_style( 'child-admin-style',
//					get_stylesheet_directory_uri() . '/admin.css');
//	wp_enqueue_style( 'child-admin-style');
}
add_action( 'admin_enqueue_scripts', 'child_admin_styles' );


function child_scripts() {

	if(is_admin()) return;

	if( is_ssl() ) {
		$protocol = "https:";
	} else {
		$protocol = "http:";
	}

//	wp_register_script( 'site-js',
//					get_stylesheet_directory_uri() . '/js/site.js',
//					array( 'jquery' ), false, true );
//	wp_enqueue_script( 'site-js' );

//	wp_register_script( 'c5-meta-forms',
//					get_stylesheet_directory_uri() . '/js/c5MetaForms.js',
//					array( 'jquery','jquery-ui-sortable','jquery-ui-datepicker' ), false, true );
//	wp_enqueue_script( 'c5-meta-forms' );
//	wp_register_script( 'c5-admin',
//					get_stylesheet_directory_uri() . '/js/c5Admin.js',
//					array( 'jquery','jquery-ui-sortable','jquery-ui-datepicker' ), false, true );
//	wp_enqueue_script( 'c5-admin' );
}
add_action('wp_enqueue_scripts', 'child_scripts' );


function child_admin_scripts() {

//	wp_enqueue_script( 'myvalidate', get_stylesheet_directory_uri() . '/js/jquery.c5validate.js' );
//	wp_enqueue_script( 'mytest', get_stylesheet_directory_uri() . '/js/test.js' );
}
add_action('admin_enqueue_scripts', 'child_admin_scripts' );


// add_filter('widget_text', 'do_shortcode');


//
//	Extend or override base frames from c5
//
function child_frame_meta_init( $frame_meta, $frame_context ) {

	//
	// define any frame data here
	//

	// do not modify below here
	$frame_names = array();
	foreach( $frame_meta as $frame ){
		$frame_names[] = $frame['name'];
	}

	$the_frame_meta = $frame_context . '_frame_data';
	if( isset( $$the_frame_meta ) ) {
		foreach ($$the_frame_meta as $frame ) {
			$frame_meta[ $frame['name'] ] = $frame;
		}
	}
	return( $frame_meta );
}
add_filter('c5_frame_meta', 'child_frame_meta_init', 10, 2);

//
// Example of override to base shortcode for header item
//
//function child_site_social( $name, $template ) {
//	$the_data = array();
//	$the_data['emailform'] = "The FORM stuff";
//	return $the_data;
//}
//
